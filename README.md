Great and powerfull tools to made with RPi3B
============================================

### As described
In some day got find that board of RPi3B I've obtained after move some components of corporative software to unused hardware, is more reliable to use in more powerfull project. On official site [pi-kvm](https://pikvm.org/) as described we can build some usefull things in day to day work with servers and other hardware. So let's starts with it.

#### DIY IP-KVM on Raspberry Pi

As for hardware part that we have.

##### Hardware for v0

  *  Raspberry Pi 2 or 3.
      ** Now I have Raspberry Pi 3B board. [Official manual](https://)
  *  MicroSD card (8 GB is enough).
      ** I got a bunch official Kingston microSD cards (and recommends to use some of them) in different capacity 2~32Gb. For this project I use one of 16Gb.
  *  USB-A 3A charger (female socket) or power supply.
      ** As for notice about micro-computer boards psu -- use good psu. With USB-A to microUSB cables and so one types of power connections -- you may have many of random troubles. It's good in experiments, not in production.
  *  For keyboard & mouse emulator (HID):
      *  Arduino Pro Micro (based on an ATMega32u4).
        ** This need to quick-buy.
      *  Logic level shifter.
        ** This need to quick-buy.
      *  1x NPN transistor (almost any NPN transistor: 2n2222 or similar).
        ** This need to quick-buy.
      *  1x 390 Ohm resistor.
        ** Have a lot of them in stock spare parts.
      *  A breadboard and wires.
        ** Have some in stock.
  *  2x USB A-to-micro cables (male-male, for power and HID).
        ** Have some in stock from random gadjets.
  *  HDMI capture device:
			* Recommended: HDMI to CSI-2 bridge based on TC358743 - low latency, more reliable, H.264 video.
        ** This need to quick-buy.
    	* ... or HDMI to USB dongle (not available for ZeroW) - high latency >200ms, not very reliable), no H.264 a very long time yet.
        ** Have this one.
  *  ATX control (optional):
    	* 4x MOSFET relays OMRON G3VM-61A1.
        ** This need to quick-buy.
    	* 4x 390 Ohm resistors.
        ** Have this in stock.
    	* 2x 4.7k Ohm resistors.
        ** Have this in stock.
    	* A breadboard and wires.
        ** Have a lot of them in stock.

##### Also something about thermal cooling

  All of them `pi boards` for one-plate PCs have onboard cpu/graphic/memory chips with high () to TDP and working temperatures. So I use for passive cooling old motherboards aluminum radiators from North/South bridges () on suitable pieces. All hardware specs, and dimensions for onboard chips can be found in official docs, board tech designs. Actually you may buy factory manufactured radiators with prepaired stick tape on it, or just use cianocrylat glue with thermal paste in it.
  Or if you don't trust passive cooling theme, you may install suitable coolers and give them power supply from board itself.

###### Thermal cooling 
  * Glue with thermal paste []()
  * Alluminium radiators hand-made and factory made (complect) []()
  * Some of () cooling systems for oneboard PCs []()

##### Addition

  *  If you want to capture VGA from your server instead of HDMI, buy the VGA-to-HDMI converter.
  *  Pi-KVM can be powered using PoE, but it is not recommend to use the official PoE HAT: it is unreliable and not compatible with the HDMI bridge. Use any other PoE hat without an I2C fan controller.
  *  Don't use random relay modules or random optocouplers! Some relays or optocouplers may not be sensitive enough for the Raspberry Pi, some others may be low-level controlled. Either use relays that are activated by a high logic level, or follow the design provided and buy an OMRON. See details here.

###### A few words about HDMI-USB dongle

  It's completely supported and Pi-KVM works great with it. But it has some disadvantages compared with recommended HDMI-CSI bridge: USB gives a lot of latency (200ms vs 100ms for MJPEG) and it doesn't support stream compression control (you won't be able to use Pi-KVM in a place with a poor internet connection). There is no H.264 support at the moment. It also cannot automatically detect screen resolution. All this is caused by the hardware limitations of the dongle itself. In addition, some users report hardware problems: the dongle may not work in the BIOS or simply stop working after a while. It's a black box, and no one knows what's inside it. If you have problems with it, it will not be possible to fix them.
  Seems that we need to make some prepairing works and buy some components.

##### Setting up v0
  ATX control board and Arduino HID (keyboard & mouse)
  [](v0.img)

##### Install the OS
  Download from official repos as they say. It's simple step, and already have been well documented.

###### Here the final steps. There are two ways to get the Pi-KVM OS:

    *  We provide the ready-made images for Raspberry Pi 4 for platforms v2-hdmi (the CSI-2 bridge) and v2-hdmiusb (the USB dongle); and for ZeroW for v2-hdmi [Follow these instructions]() to install the OS quickly.
    *  For the other boards and platforms, you need to build the operating system manually. Don't worry, it's very simple! [Just follow these instructions](). You can also build the OS for RPi4 manually if you really want to :)

##### Final steps
    Congratulations! Your Pi-KVM will be available via SSH (ssh root@<addr> with password root by default) and HTTPS (try to open in a browser the URL https://<addr>, the login admin and password admin by default). For HTTPS a self-signed certificate is used by default.

    To change the root password use command passwd via SSH or webterm. To change Pi-KVM web password use kvmd-htpasswd set admin. As indicated on the login screen use rw to make the root filesystem writable, before issuing these commands. After making changes, make sure to run the command ro.

##### Remote access to pi-kvm board
    To make easy access to Pi-KVM from the Internet you may publish some bunch of ports on your core-router, build VPN/ssh tunnel or something else too. Your imagination for this is free to deploy.
    
    Port forwarding for port 443 to Pi-KVM (if you has an external IP address). In all other cases, you can use the excellent free VPN service Tailscale, which is configured on Pi-KVM with a few simple commands.

    If you have any problems or questions, contact us using Discord: https://discord.gg/bpmXfz5
    Subscribe to our Subreddit to follow news and releases: https://www.reddit.com/r/pikvm

    Also, you feel free to contact not official group (as authors of this manual) in Telegram http://t.me/pi-kvm-modelling or email me "Lain Iwakura" <layne.iwakura@gmail.com>
